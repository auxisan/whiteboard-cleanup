// ConnectedComponent.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "opencv2\core\core.hpp"
#include "opencv2\highgui\highgui.hpp"
#include "opencv2\imgproc\imgproc.hpp"
#include "opencv2\opencv.hpp"
#include <iostream>

using namespace cv;
using namespace std;

int main ()
{
	/*
    if (argc != 2)
    {
            std::cout << "USE: " << argv[0] << " <file_path>" << std::endl;
            return -1;
    }
	*/
    //Input color image
    Mat imageMat = cv::imread("Foreground.jpg", CV_LOAD_IMAGE_COLOR);
    if (imageMat.empty())
    {
            std::cerr << "ERROR: Could not read image " << std::endl;
            return -1;
    }

    //Grayscale matrix
    Mat grayscaleMat (imageMat.size(), CV_8U);

    //Convert BGR to Gray
    cvtColor( imageMat, grayscaleMat, CV_BGR2GRAY );

    //Binary image
    Mat binaryMat(grayscaleMat.size(), grayscaleMat.type());

    //Apply thresholding
    threshold(grayscaleMat, binaryMat, 100, 255, cv::THRESH_BINARY);
	//Mat srcComponentMat = binaryMat;
    
	// Connected componenet labelling

	Mat componentMat = Mat::zeros(binaryMat.rows, binaryMat.cols, CV_8UC3);
	vector<vector<Point> > contours;
	vector<Vec4i> hierarchy;
	
	findContours( binaryMat, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE );
	// iterate through all the top-level contours,
	// draw each connected component with its own random color
	int idx = 0;
	for( ; idx >= 0; idx = hierarchy[idx][0] )
	{
		Scalar color( rand()&255, rand()&255, rand()&255 );
		drawContours( componentMat, contours, idx, color, CV_FILLED, 8, hierarchy );
	}
			
	//Show the results
	namedWindow("Input", cv::WINDOW_NORMAL);
	imshow("Input", imageMat);

	namedWindow( "Components", cv::WINDOW_NORMAL );
	imshow( "Components", componentMat );

    cv::waitKey(0);
    return 0;
}
